scalaVersion in ThisBuild := "2.12.3"

sonatypeGithost := (Gitlab, "fommil", "sbt-sensible")
licenses := Seq(Apache2)
