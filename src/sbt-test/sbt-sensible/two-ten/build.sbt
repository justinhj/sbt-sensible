scalaVersion in ThisBuild := "2.10.6"

sonatypeGithost := (Gitlab, "fommil", "sbt-sensible")
licenses := Seq(Apache2)
